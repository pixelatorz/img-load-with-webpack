FROM nginx

ENV DEBIAN_FRONTEND noninteractive

WORKDIR /website_files

RUN rm -rf *

ADD ./dist /var/www
ADD ./docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf
