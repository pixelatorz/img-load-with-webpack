const fs = require('fs');
const path = require('path');
const async = require('async');

const getDirectories = require('./util/get-directories');
const { gettextToI18next } = require('i18next-conv');

const locales = getDirectories('./src/i18n/locales');

async.each(
  locales,
  (locale, callback) => {
    const jsonFilePath = path.join(process.cwd(), 'src/i18n/locales', locale, 'translation.json');
    const poFilePath = path.join(process.cwd(), 'src/i18n/locales', locale, 'translation.po');
    fs.readFile(poFilePath, function (err, poFile) {
      gettextToI18next(locale, poFile.toString()).then((result, err) => {
        if (err) {
          callback(err);
        }

        const parsedJson = JSON.parse(result.toString());
        const fileContents = `${JSON.stringify(parsedJson, null, 2)}\n`;
        fs.writeFile(jsonFilePath, fileContents, callback);
      });
    });

  },
  (err) => {
    if (err) {
      throw err;
    }

    console.log(`Done converting ${locales.length} locales from .json to .po`);
  }
);

