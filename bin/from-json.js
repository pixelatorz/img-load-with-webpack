const fs = require('fs');
const path = require('path');
const async = require('async');

const getDirectories = require('./util/get-directories');
const { i18nextToPo } = require('i18next-conv');

const locales = getDirectories('./src/i18n/locales');

async.each(
  locales,
  (locale, callback) => {
    const jsonFilePath = path.join(process.cwd(), 'src/i18n/locales', locale, 'translation.json');
    const poFilePath = path.join(process.cwd(), 'src/i18n/locales', locale, 'translation.po');
    fs.readFile(jsonFilePath, function (err, jsonFile) {
      i18nextToPo(locale, jsonFile.toString()).then((result, err) => {
        fs.writeFile(poFilePath, result, callback);
      });
    });

  },
  (err) => {
    if (err) {
      throw err;
    }

    console.log(`Done converting ${locales.length} locales from .json to .po`);
  }
);

