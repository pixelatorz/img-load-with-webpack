export default function createAsyncAction({asyncRequest, payload, types}) {
  return (dispatch) => {
    dispatch({
      type: types.request,
      payload: payload
    });

    return asyncRequest()
      .then(data => {
        dispatch({
          type: types.success,
          payload: data
        });
      })
      .catch(error => {
        dispatch({
          type: types.error,
          payload: error
        });
      })
    ;
  }
}