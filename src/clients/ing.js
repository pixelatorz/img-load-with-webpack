const mock = true;

import mockGetAllBranches from '../mocks/getAllBranches.json';

/**
 * @param {string} language
 *
 * @returns {Promise}
 */
function getAllBranches(language) {
  if (mock) {
    return doMockRequest('getAllBranches', mockGetAllBranches, false)
  }

  return doRequest('getAllBranches', {
    'language': language,
  })
}

/**
 * @param {string} search
 * @param {string} language
 * @param {Object} position
 * @param {Object} query
 *
 * @returns {Promise}
 */
function getBranchesInRange(search, language, position, query) {
  let body = {
    'search': search,
    'language': language,
    'currentLongitude': position.longitude,
    'currentLatitude': position.latitude,
    'selfbank': query.selfbank || null,
    'cash': query.cash || null,
    'cashin': query.cashin || null,
    'parking': query.parking || null,
    'safe': query.safe || null,
    'access': query.access || null,
    'night': query.night || null,
    'saturday': query.saturday || null,
  };

  if (mock) {
    return doMockRequest('getBranchesInRange', mockGetAllBranches, false);
  }

  return doRequest('getBranchesInRange', body);
}

/**
 * @param {string} search
 * @param {string} language
 * @param {Object} position
 * @param {Object} query
 *
 * @returns {Promise}
 */
function getBranchesInRangeWithCoordinates(search, language, position, query) {
  let body = {
    'search': search,
    'language': language,
    'currentLongitude': position.longitude,
    'currentLatitude': position.latitude,
    'selfbank': query.selfbank || null,
    'cash': query.cash || null,
    'cashin': query.cashin || null,
    'parking': query.parking || null,
    'safe': query.safe || null,
    'access': query.access || null,
    'night': query.night || null,
    'saturday': query.saturday || null,
  };

  return doRequest('getBranchesInRangeWithCoordinates', body);
}

/**
 * @param {string} guid
 * @param {string} language
 */
function getBranchInfo(guid, language) {
  return doRequest('getBranchInfo', {
    'guid': guid,
    'language': language,
  })
}

/**
 * @param {string} name
 * @param {string} language
 */
function getBranchInfoByName(name, language) {
  return doRequest('getBranchInfo', {
    'name': guid,
    'language': language,
  })
}

/**
 * @param {string} action
 * @param {Object} body
 *
 * @returns {Promise}
 */
function doRequest(action, body) {

  let headers = new Headers();

  headers.set('Accept', 'application/json');
  headers.set('Content-Type', 'application/json');

  const params = {
    body: {
      method: action,
      params: body
    },
    headers: headers,
    method: 'POST'
  };

  let request = new Request('https://branches.ing.be/ws/WSBranchfinder.ashx', params);

  return fetch(request)
    .then(response => {
      if (!response.ok) {
        return response.json().then(Promise.reject.bind(Promise));
      }

      return response.json();
    })
    .catch(error => {
      throw error;
    })
  ;
}

/**
 * @param {string}  action
 * @param {object}  wantedResponse
 * @param {boolean} fail
 * @returns {Promise}
 */
function doMockRequest(action, wantedResponse, fail = false) {
  console.log(`Fired mock response for ${action}`);
  console.dir(wantedResponse);

  return new Promise((resolve, reject) => {
    if (fail) {
      window.setTimeout(function () {
        reject(wantedResponse)
      }, 1000)
    }

    if (!fail) {
      window.setTimeout(function () {
        resolve(wantedResponse)
      }, 1000)
    }
  }).then(response => {
    return response
  });
}

export {
  getAllBranches,
  getBranchesInRange,
  getBranchesInRangeWithCoordinates,
  getBranchInfo,
  getBranchInfoByName
}