const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

var path = require('path');

module.exports = [
    {
        entry: "./src/app.js",
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: "js/bundle.js",
            publicPath: "/dist"
        },
        resolve: {
            modules: ["node_modules", __dirname + "/"]
        },
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["react", "es2015", "stage-1"]
                }
            }, {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'file-loader?name=[name].[ext]&outputPath=../images/&limit=1000000&useRelativePath=true',
                    'image-webpack-loader'
                ]
            }]
        },
        plugins: [
            new CopyWebpackPlugin([
                {
                    context: './src/static',
                    from: '**/*'
                }
            ])
        ]
    },
    {
        entry: "./style/style.scss",
        watch: true,

        output: {
            path: path.resolve(__dirname, 'dist/css'),
            filename: "style.css",
            publicPath: "/dist/css"
        },
        resolve: {
            modules: ["node_modules"]
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        //resolve-url-loader may be chained before sass-loader if necessary
                        use: ['css-loader', 'sass-loader']
                    }),
                },{
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        'file-loader?name=[name].[ext]&outputPath=../images/&limit=1000000&useRelativePath=true',
                        'image-webpack-loader'
                    ]
                }
            ]
        },
        plugins: [
            new ExtractTextPlugin('style.css')
            //if you want to pass in options, you can do so:
            //new ExtractTextPlugin({
            //  filename: 'style.css'
            //})
        ]
    }
];
